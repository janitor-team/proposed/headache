<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" 
  "/usr/share/xml/docbook/schema/dtd/4.4/docbookx.dtd" [
  <!ENTITY debian      "Debian GNU/Linux">
  <!ENTITY dhprg       "<command>headache</command>">
]>

<!--**********************************************************************-->
<!-- Headache manpage                                                     -->
<!--                                                                      -->
<!-- Copyright (C) 2003-2006 Sylvain Le Gall <gildor@debian.org>          -->
<!--                                                                      -->
<!-- This library is free software; you can redistribute it and/or        -->
<!-- modify it under the terms of the GNU Lesser General Public           -->
<!-- License as published by the Free Software Foundation; either         -->
<!-- version 2.1 of the License, or (at your option) any later version;   -->
<!-- with the OCaml static compilation exception.                         -->
<!--                                                                      -->
<!-- This library is distributed in the hope that it will be useful,      -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of       -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    -->
<!-- Lesser General Public License for more details.                      -->
<!--                                                                      -->
<!-- You should have received a copy of the GNU Lesser General Public     -->
<!-- License along with this library; if not, write to the Free Software  -->
<!-- Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,               -->
<!-- MA 02110-1301, USA.                                                  -->
<!--                                                                      -->
<!-- Contact: gildor@debian.org                                           -->
<!--**********************************************************************-->

<refentry>

  <refentryinfo>
    <author>
      <firstname>Sylvain</firstname>
      <surname>Le Gall</surname>
      <email>gildor@debian.org</email>
      </author>
      <copyright>
        <year>2003, 2004, 2005, 2006</year>
        <holder>Sylvain Le Gall</holder>
      </copyright>
      <date>Feb 15, 2004</date>
    </refentryinfo>
    
    <refmeta>
      <refentrytitle>HEADACHE</refentrytitle>
      <manvolnum>1</manvolnum>
    </refmeta>
    
    <refnamediv>
      <refname>&dhprg;</refname>

      <refpurpose>A program to manage the license of your source
        file.</refpurpose>
    </refnamediv>
    
    <refsynopsisdiv>
      <cmdsynopsis>
        &dhprg; 
        <arg>-h 
          <replaceable>file</replaceable>
        </arg>
        <arg>-c 
          <replaceable>file</replaceable>
        </arg>
        <arg>-r</arg>
        <group>
          <arg choice="plain">-help</arg>
          <arg choice="plain">--help</arg>
        </group>
        <arg choice="plain" rep="repeat">
          <replaceable>file</replaceable>
        </arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the &dhprg; command.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.</para>
  
    <para>It is a common usage to put at the beginning of source code files a
      short header giving, for instance, some copyright information. &dhprg;
      is a simple and lightweight tool for managing easily these headers.
      Among its functionalities, one may mention:
    
      <itemizedlist>
        <listitem>
          <para>Headers must generally be generated as comments in source  code
            files. &dhprg; deals with different files types and generates  for
            each of them headers in an appropriate format.</para>
          </listitem>
          <listitem>
            <para>Headers automatically detects existing headers and removes them. 
              Thus, you can use it to update headers in a set of files.</para>
          </listitem>
      </itemizedlist>
    </para>
  </refsect1>
  
  <refsect1>
    <title>COMMAND LINE OPTIONS</title>
  
    <variablelist>
      <varlistentry>
        <term><filename>file</filename></term>
        <listitem>
          <para>Name of one file to process</para>
        </listitem>
      </varlistentry>    
      <varlistentry>
        <term>-h <filename>file</filename></term>
        <listitem>
          <para>Create a header with text coming from 
            <filename>file</filename></para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>-c <filename>file</filename></term>
        <listitem>
          <para>Read the given configuration
            <filename>file</filename></para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>-help</term>
        <term>--help</term>
        <listitem>
          <para>Display the option list of &dhprg;</para>
        </listitem>
      </varlistentry>
    </variablelist>
  
  </refsect1>
  
  <refsect1>
    <title>USAGE</title>

    <para>Let us illustrate the use of this tool with a small example. Assume
      you have a small project mixing C and Caml code consisting in three
      files 'foo.c', 'bar.ml' and 'bar.mli'', and you want to equip them with
      some header. First of all, write a header  file, i.e. a plain text file
      including the information headers must mention. An example of such a
      file is given in figure 1. In the following, we assume this file is
      named 'myheader' and is in the same directory as source files.</para>

    <para>Then, in order to generate headers, just run the command : 
      <command>&dhprg; -h <filename>myheader</filename>
        <filename>foo.c</filename>
        <filename>bar.ml</filename>
        <filename>bar.mli</filename>
      </command>
    </para>

    <para>Each file is equipped with an header including the text given in the
      header file 'myheader', surrounded by some extra characters depending on
      its format making it a comment (e.g. '(*' and '*)' in '.ml' files). If
      you update the header file 'myheader', you simply need to re-run the above
      command to update headers in source code files.</para>
    
    <para>Similarly, running :
      <command>&dhprg; -r 
        <filename>foo.c</filename>
        <filename>bar.ml</filename>
        <filename>bar.mli</filename>
      </command>
      removes any existing in files 'foo.c', 'bar.ml' and 'bar.mli'. Files
      which do not have a header are kept unchanged.</para>

  </refsect1>

  <refsect1>
    <title>CONFIGURATION FILE</title>

    <para>File types and format of header may be specified by a configuration
      file. By default, the default builtin configuration file given in figure
      2 is used. You can also use your own configuration file thanks to the
      <option>-c</option> option : 
      <command>&dhprg; 
        -c <filename>myconfig</filename>
        -h <filename>myheader</filename>
        <filename>foo.c</filename>
        <filename>bar.ml</filename>
        <filename>bar.mli</filename>
      </command></para>
    
    <para>In order to write your own configuration, you can follow the example
      given in figure 2. A configuration file consists in a list of entries
      separated by the character '|'. Each of them is made of two parts
      separated by an '->'.</para>
    
    <para>The first one is a regular expression. (Regular  expression are
      enclosed within double quotes and have the same  syntax as in Gnu
      Emacs.) &dhprg; determines file types according to  file basenames;
      thus, each file is dealt with using the first line  its name matches.
    </para>
    
    <para>The second one describes the format of headers for files of this 
      type. It consists of the name of a model (e.g.   'frame'), possibly
      followed by a list of arguments. Arguments  are named: 'open:"(*"'
      means that the value of the argument  'open' is '(*'. 
      &dhprg; currently supports three models and a special keyword:</para>
      
    <variablelist>
      <varlistentry>
        <term>frame</term>
        <listitem>
          <para>With this model, headers are generated in a  frame. This
            model requires three arguments: 'open' and  'close' (the opening and
            closing sequences for comments) and  'line' (the character used to
            make the horizontal lines of the  frame). Two optional arguments may
            be used 'margin' (a string  printed between the left and right side
            of the frame and the border,  by default two spaces) and 'width' (the
            width of the inside of  the frame, default is 68).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>lines</term>
        <listitem>
          <para>Headers are typeset between two lines. Three  arguments must
            be provided: 'open' and 'close' (the  opening and closing sequences
            for comments), 'line' (the  character used to make the horizontal
            lines). Three optional  arguments are allowed: 'begin' (a string
            typeset at the  beginning of each line, by default two spaces),
            'last' (a  string typeset at the beginning of the last line) and
            'width'  (the width of the lines, default is 70).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>no</term>
        <listitem>
          <para>This model generates no header and has no argument.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>skip</term>
        <listitem>
          <para>Skip line corresponding to one of the "match" parameters
            regexp. For this kind of line, every first part pattern that matches
            the file basename is taken into account.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1>
    <title>SEE ALSO</title>

    <para><filename>/usr/share/doc/headache/manual.html</filename></para>
  </refsect1>

  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by 
      <personname>
        <firstname>Sylvain</firstname>
        <surname>Le Gall</surname>
      </personname>
      <email>gildor@debian.org</email> 
      Permission is granted to copy, distribute and/or modify this document
      under the terms of the <acronym>GNU</acronym> Lesser General Public
      License, Version 2.1 or any later version published by the Free Software
      Foundation; considering as source code all the file that enable the
      production of this manpage.</para>

  </refsect1>

</refentry>





